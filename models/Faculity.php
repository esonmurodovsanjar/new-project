<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faculity".
 *
 * @property int $id
 * @property string $name Millat
 *
 * @property Students[] $students
 */
class Faculity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faculity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 30],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Fakultet',
            'id' => 'ID',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Students::className(), ['faculty_id' => 'id']);
    }
}
