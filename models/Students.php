<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "students".
 *
 * @property int $id
 * @property string $fio ismi familyasi,sharifi
 * @property string $telephone nomerni kiriting
 * @property string $birthday tug'ilgan kuningizni kiriting
 * @property int $gender jinsi
 * @property int $region_id
 * @property int $course kursini kiriting
 * @property int $national_id
 * @property int $faculty_id
 * @property string $description
 *
 * @property Faculity $faculty
 * @property Nationally $national
 * @property Regions $region
 */
class Students extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'students';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birthday'], 'safe'],
            [['gender', 'region_id', 'course', 'national_id', 'faculty_id'], 'integer'],
            [['fio'], 'string', 'max' => 70],
            [['telephone'], 'string', 'max' => 15],
            [['description'], 'string', 'max' => 255],
            [['faculty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Faculity::className(), 'targetAttribute' => ['faculty_id' => 'id']],
            [['national_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nationally::className(), 'targetAttribute' => ['national_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Regions::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {

        
        return [
            'id' => 'ID',
            'fio' => 'Ismi, Familyasi,Sharifi',
            'telephone' => 'Nomerni kiriting',
            'birthday' => 'Tug\'ilgan kuningizni kiriting',
            'gender' => 'Jinsi',
            'region_id' => 'Region ID',
            'course' => 'Kursini kiriting',
            'national_id' => 'Millat ID',
            'faculty_id' => 'Fakultet ID',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaculty()
    {
        return $this->hasOne(Faculity::className(), ['id' => 'faculty_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNational()
    {
        return $this->hasOne(Nationally::className(), ['id' => 'national_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['id' => 'region_id']);
    }

    public function getRegionsList()
    {
        $region = Regions::find()->all();
        return ArrayHelper::map($region, 'id', 'name');
    }
    public function getGender()
    {
        $region=["Erkak","Ayol"];
        return $region;
    }
    public function getNationallyList()
    {
        $national=Nationally::find()->all();
        return ArrayHelper::map($national,'id','name');
    }
     public function getFacultyList()
    {
        $faculty=Faculity::find()->all();
        return ArrayHelper::map($faculty,'id','name');
    }
}
