<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Students */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="students-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="row">
        <div class="col-md-4 col-sm-4 col-lg-4">
             <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-sm-4 col-lg-4">
          <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-sm-4 col-lg-4">
          <?= $form->field($model, 'birthday')->textInput() ?>
        </div>
    </div>
 

    <div class="row">
        <div class="col-md-4">
    <?= $form->field($model, 'gender')->dropDownList($model->getGender(),[]) ?>
        </div>
        <div class="col-md-4">
    <?= $form->field($model, 'region_id')->dropDownList($model->getRegionsList(),[]) ?>
        </div>
        <div class="col-md-4">
    <?= $form->field($model, 'course')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
         <?= $form->field($model, 'national_id')->dropDownList($model->getNationallyList(),[]) ?>  
         </div>      
         <div class="col-md-4">
    <?= $form->field($model, 'faculty_id')->dropDownList($model->getFacultyList(),[]) ?>  
         </div> 
         <div class="col-lg-4">
    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
        </div>

  </div>
    








    <div class="form-group">
        <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
