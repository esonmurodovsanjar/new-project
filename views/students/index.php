<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Talabalar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="students-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('talabalarni kirtish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fio',
            'telephone',
            'birthday',
             [
                'attribute' => 'gender',
                'content' => function($data){
                    if($data->gender==0) return "Erkak";
                    else return "Ayol";
                }
            ],
            [
                'attribute' => 'region_id',
                'value' => function($data){
                    return $data->region->name;
                }
            ],
            'course',
            [
                'attribute' => 'national_id',
                'value' => function($data){
                    return $data ->national->name;
                }
            ],
               [
                'attribute' => 'faculty_id',
                'value' => function($data){
                    return $data ->faculty->name;
                }
            ],
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
