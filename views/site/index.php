<?php

/* @var $this yii\web\View */

$this->title = 'Mening dasturim';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Salom Xujayin!</h1>
        
        <p class="lead">Siz Yii-2 ni omadli ochdingiz.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">YII-2 ni boshlaymiz</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h3>Bulim</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/" >yii  dokumentiga utish &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h3>Bulim</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forumiga utish &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h3>Bulim</h3>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii mashqlariga utish &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
