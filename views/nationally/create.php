<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nationally */

$this->title = 'Millatni kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Millatlar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nationally-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
