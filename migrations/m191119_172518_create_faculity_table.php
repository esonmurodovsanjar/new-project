<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%faculity}}`.
 */
class m191119_172518_create_faculity_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%faculity}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(30)->unique()->comment('Millat'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%faculity}}');
    }
}
