<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%students}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%regions}}`
 * - `{{%nationally}}`
 * - `{{%faculity}}`
 */
class m191119_175223_create_students_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%students}}', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(70) ->comment('ismi familyasi,sharifi'),
            'telephone' => $this->string(15) ->comment('nomerni kiriting'),
            'birthday' => $this ->date() -> comment("tug'ilgan kuningizni kiriting"),
            'gender' => $this -> boolean() ->comment('jinsi'),

            'region_id' => $this->integer(),
            'course' => $this ->integer()->comment("kursini kiriting"),
            'national_id' => $this->integer(),
            'faculty_id' => $this->integer(),
        ]);

        // creates index for column `region_id`
        $this->createIndex(
            '{{%idx-students-region_id}}',
            '{{%students}}',
            'region_id'
        );

        // add foreign key for table `{{%regions}}`
        $this->addForeignKey(
            '{{%fk-students-region_id}}',
            '{{%students}}',
            'region_id',
            '{{%regions}}',
            'id',
            'CASCADE'
        );

        // creates index for column `national_id`
        $this->createIndex(
            '{{%idx-students-national_id}}',
            '{{%students}}',
            'national_id'
        );

        // add foreign key for table `{{%nationally}}`
        $this->addForeignKey(
            '{{%fk-students-national_id}}',
            '{{%students}}',
            'national_id',
            '{{%nationally}}',
            'id',
            'CASCADE'
        );

        // creates index for column `faculty_id`
        $this->createIndex(
            '{{%idx-students-faculty_id}}',
            '{{%students}}',
            'faculty_id'
        );

        // add foreign key for table `{{%faculity}}`
        $this->addForeignKey(
            '{{%fk-students-faculty_id}}',
            '{{%students}}',
            'faculty_id',
            '{{%faculity}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%regions}}`
        $this->dropForeignKey(
            '{{%fk-students-region_id}}',
            '{{%students}}'
        );

        // drops index for column `region_id`
        $this->dropIndex(
            '{{%idx-students-region_id}}',
            '{{%students}}'
        );

        // drops foreign key for table `{{%nationally}}`
        $this->dropForeignKey(
            '{{%fk-students-national_id}}',
            '{{%students}}'
        );

        // drops index for column `national_id`
        $this->dropIndex(
            '{{%idx-students-national_id}}',
            '{{%students}}'
        );

        // drops foreign key for table `{{%faculity}}`
        $this->dropForeignKey(
            '{{%fk-students-faculty_id}}',
            '{{%students}}'
        );

        // drops index for column `faculty_id`
        $this->dropIndex(
            '{{%idx-students-faculty_id}}',
            '{{%students}}'
        );

        $this->dropTable('{{%students}}');
    }
}
