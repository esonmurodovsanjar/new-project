<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%nationally}}`.
 */
class m191119_172458_create_nationally_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%nationally}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(30)->unique()->comment('Millat'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%nationally}}');
    }
}
